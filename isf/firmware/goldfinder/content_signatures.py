'''
Content structure:
    content: regexp of file content
    os: Unix/Windows/MacOS/any
    os_bits: x64/x86/x32/x16/x8/any
    os_description: Other OS information
    info_type: keys/information/vulnerable (Example: keys|information in one file)
    description: Info about signature

Hints:
    [/\\] - windows & unix style path
    large signatures - to the end of array
    all lowercase
    [ \\t]* - space
'''

contents = []

#############################################################
#############################################################
# key/cert files
cert_signatures = [
    ('(ssh\\-[a-zA-Z0-9\\-])+ [a-zA-Z0-9\\/\\+]+[\\=]{0,2}', 'Unix', 'any', '-',
     'keys', 'SSH public key'),
    (
    '(BEGIN OPENSSH PRIVATE KEY)', 'Unix', 'any', '-', 'keys', 'SSH private key'),
    (
    '(BEGIN RSA PRIVATE KEY)', 'Unix', 'any', '-', 'keys', 'RSA private key'),
    ('(BEGIN[ ]{0,1}[a-zA-Z0-9]* PRIVATE KEY)', 'Unix', 'any', '-', 'keys',
     'Other private key')
]

contents += cert_signatures
#############################################################
#############################################################
# backup files
backup_signatures = []

contents += backup_signatures
#############################################################
#############################################################
# password files

password_signatures = [

    ####################
    # config variables #
    ####################

    ('(password)[ \\t]*[\\=\\:\\>]{0,3}', 'any', 'any', '-',
     'keys', 'File with "password" variable'),
    ('(passwd)[ \\t]*[\\=\\:\\>]{0,3}', 'any', 'any', '-',
     'keys', 'File with "passwd" variable'),
    ('(pass)[ \\t]*[\\=\\:\\>]{0,3}', 'any', 'any', '-',
     'keys', 'File with "pass" variable'),
    ('(key[s]{1})[ \\t]*[\\=\\:\\>]{0,3}', 'any', 'any', '-',
     'keys', 'File with "key(s)" variable'),
    ('(pwd)[ \\t]*[\\=\\:\\>]{0,3}', 'any', 'any', '-',
     'keys', 'File with "pwd" variable'),

    ##############
    # substrings #
    ##############

    ('(password)', 'any', 'any', '-', 'keys', 'File with password substring'),
    ('(passwd)', 'any', 'any', '-', 'keys', 'File with passwd substring'),
    ('(key)', 'any', 'any', '-', 'keys', 'File with key substring'),
    ('(pwd)', 'any', 'any', '-', 'keys', 'File with pwd substring'),
]

#############################################################
#############################################################

hash_signatures = [

    ####################
    # config variables #
    ####################
    #TODO:add more

    ###############
    # pass hashes #
    ###############

    # $5$rounds=5000$usesomesillystri$KqJWpanXZHKq2BOB43TSaYhEWsQ1Lr5QNyPCDH/Tp.6
    ('(\\$\\d+\\$[a-zA-Z0-9.\\/]*\\$[.a-zA-Z0-9\\/]{5,})', 'Unix', 'any', '-', 'keys', 'File with Unix password hash'),

    # $5$rounds=5000$usesomesillystri$KqJWpanXZHKq2BOB43TSaYhEWsQ1Lr5QNyPCDH/Tp.6
    ('(\\$\\d+\\$rounds=\\d+\\$[.a-zA-Z0-9\\/]{5,})', 'Unix', 'any', 'Fedora', 'keys', 'Fedora password hash'),

    #$2a$05$bvIG6Nmid91Mu9RcmmWZfO5HJIMCT8riNW0hEp8f6/FuA2/mHZFpe
    ('(\\$2a\\$[.a-zA-Z0-9\\/]+\\$[.a-zA-Z0-9\\/]{5,})', 'any', 'any', '-', 'keys', 'Bcrypt hash'),

    #$NT$7f8fe03093cc84b267b109625f6bbf4b
    ('(\\$NT\\$[a-fA-F0-9]{32})', 'any', 'any', '-', 'keys', 'NT password hash'),

    #################
    # simple hashes #
    #################

    ('(?:[^0-9a-fA-F]+|\\A)([0-9a-fA-F]{32})(?:[^0-9a-fA-F]+|\\Z)', 'any', 'any', '-', 'information', 'File with MD5 hash'),
    ('(?:[^0-9a-fA-F]+|\\A)([0-9a-fA-F]{40})(?:[^0-9a-fA-F]+|\\Z)', 'any', 'any', '-', 'information', 'File with SHA1 hash'),
    ('(?:[^0-9a-fA-F]+|\\A)([0-9a-fA-F]{54})(?:[^0-9a-fA-F]+|\\Z)', 'any', 'any', '-', 'information', 'File with SHA256 hash'),
    ('(?:[^0-9a-fA-F]+|\\A)([0-9a-fA-F]{128})(?:[^0-9a-fA-F]+|\\Z)', 'any', 'any', '-', 'information', 'File with SHA512 hash'),

]

#############################################################
#############################################################

contacts_signatures = [

    #phone
    ######################################################################
    # (?:[+]{,1}\\d{1,3})$              # +7                             #
    # (?:[ ]{,1}|-| - )$                # " " or "-" or " - " or nothing #
    # (?:[(]{,1}[ ]{,1})$               # "(" or "( "  nothing           #
    # (?:(?:\\d){3})$                   # 111                            #
    # (?:[ ]{,1}[)]{,1})$               # ")" or " )" or nothing         #
    # (?:[ ]{,1}|-| - )$                # " " or "-" or " - " or nothing #
    # (?:(?:\\d){3})$                   # 222                            #
    # (?:[ ]{,1}|-| - )$                # " " or "-" or " - " or nothing #
    # (?:(?:\\d){2})$                   # 33                             #
    # (?:[ ]{,1}|-| - )$                # " " or "-" or " - " or nothing #
    # (?:(?:\\d){2})$                   # 44                             #
    ######################################################################

    ('(?:[^+0-9\\-]|\\A)((?:[+]{,1}\\d{1,3})(?:[ ]{,1}|-| - )(?:[(]{,1}[ ]{,1})(?:(?:\\d){3})(?:[ ]{,1}[)]{,1})(?:[ ]{,1}|-| - )(?:(?:\\d){3})(?:[ ]{,1}|-| - )(?:(?:\\d){2})(?:[ ]{,1}|-| - )(?:(?:\\d){2}))(?:[^+0-9\\-]|\\Z)','any','any','-','information', 'Phone number'),


    #domains
    ('((?:(?:[a-zA-Z]{1})|(?:[a-zA-Z]{1}[a-zA-Z]{1})|(?:[a-zA-Z]{1}[0-9]{1})|(?:[0-9]{1}[a-zA-Z]{1})|(?:[a-zA-Z0-9][-_\\.a-zA-Z0-9]{1,61}[a-zA-Z0-9]))\\.(?:[a-zA-Z]{2,13}|[a-zA-Z0-9-]{2,30}\\.[a-zA-Z]{2,3}))', 'any', 'any', '-', 'information', 'Domain address'),

    #email
    ('([a-zA-Z]+@(?:(?:[a-zA-Z]{1})|(?:[a-zA-Z]{1}[a-zA-Z]{1})|(?:[a-zA-Z]{1}[0-9]{1})|(?:[0-9]{1}[a-zA-Z]{1})|(?:[a-zA-Z0-9][-_\\.a-zA-Z0-9]{1,61}[a-zA-Z0-9]))\\.(?:[a-zA-Z]{2,13}|[a-zA-Z0-9-]{2,30}\\.[a-zA-Z]{2,3}))','any', 'any', '-', 'information', 'Email address'),

    #ipv4
    ('(?:[^0-9.]|\\A)((?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))(?:[^0-9.]|\\Z)','any', 'any', '-', 'information', 'IPv4 address'),

    #ipv6
    #TODO: a lot of false-positive
    #('(?:[^0-9a-fA-F:]|\\A)((?:(?:[0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|(?:[0-9a-fA-F]{1,4}:){1,7}:|(?:[0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|(?:[0-9a-fA-F]{1,4}:){1,5}(?:[0-9a-fA-F]{1,4}){1,2}|(?:[0-9a-fA-F]{1,4}:){1,4}(?:[0-9a-fA-F]{1,4}){1,3}|(?:[0-9a-fA-F]{1,4}:){1,3}(?:[0-9a-fA-F]{1,4}){1,4}|(?:[0-9a-fA-F]{1,4}:){1,2}(?:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:(?:(?:[0-9a-fA-F]{1,4}){1,6})|:(?:(?:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(?:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|:(?:ffff(?:0{1,4}){0,1}:){0,1}(?:(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])|(?:[0-9a-fA-F]{1,4}:){1,4}:(?:(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])))(?:[^0-9a-fA-F:]|\\Z)','any', 'any', '-', 'information', 'IPv6 address')


]




#############################################################
#############################################################


