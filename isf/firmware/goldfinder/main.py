from isf.core import logger
from .content_signatures import *
from .filename_signatures import *
import os
import re
from .ignore_rules import *


class GoldFinder:
    path = ''
    dirs_list = []  # without \ at the end
    files_list = []
    to_return = True
    ignore_ext = True

    def __init__(self, path=".\\test_firmware\\", console_mode=True,
                 ignore_ext=True):
        if not os.path.exists(path):
            logger.error('Directory {} doesn\'t exist'.format(path))
            self.__del__()
            return
        self.path = path
        self.to_return = not console_mode
        self.ignore_ext = ignore_ext
        # if file in args
        if os.path.isfile(path):
            self.files_list = [path]
            return
        for r, d, f in os.walk(path):
            for file in f:
                self.files_list.append(os.path.join(r, file))
            for directory in d:
                self.dirs_list.append(os.path.join(r, directory))

    def __del__(self):
        pass

    # TODO: OS detection
    def detect_system(self):
        pass

    #####################
    # Checker functions #
    #####################

    def file_checker(self,
                     file_path,
                     name_signature_array,
                     extention_signature_array,
                     content_signature_array,
                     ignore_extentions,
                     priority=(0, 1, 2),
                     find_all=0,
                     dublicates=0):
        '''
        :param file_path: path to file for checking
        :param name_signature_array: name signature array from config
        :param extention_signature_array: extention sign array from config
        :param content_signature_array: content sign array from config
        :param priority: array with sequence nums 0 - name, 1 -ext, 2 - content
        :param ignore_extentions: Example: ['.html','.htm']
        :return:
        '''
        if self.ignore_ext and sum(
                [file_path.lower().endswith(extention[0]) for extention in
                 ignore_extentions]) > 0:
            logger.debug('File {} was ignored!'.format(file_path))
            return []

        logger.debug('Checking file {}'.format(file_path))

        def name_check():
            result = []
            logger.debug('Checking name of file')
            for name_signature in name_signature_array:
                if re.match(name_signature[0], file_path.lower()):
                    result.append(
                        {'name_signature': name_signature, 'found_str': []})
                    if not dublicates:
                        return result
            return []

        def extention_check():
            logger.debug('Checking extention of file')
            result = []
            for extention in extention_signature_array:
                logger.debug(
                    'File: {}, check_extention: {}'.format(file_path.lower(),
                                                           str(extention)))
                if file_path.lower().endswith(extention[0]):
                    result.append(
                        {'extention_signature': extention, 'found_str': []})
                    if not dublicates:
                        return result
            return result

        def content_check():
            full_content_result = []
            try:
                f = open(file_path, 'rb')
                file_content = f.read().decode('charmap')
                f.close()
                for content in content_signature_array:
                    if re.search(content[0], file_content):

                        if find_all:
                            found_arr = list(
                                set(re.findall(content[0], file_content)))
                            full_content_result.append(
                                {'content_signature': content,
                                 'found_str': found_arr})

                        else:
                            full_content_result.append(
                                {'content_signature': content,
                                 'found_str': []})
                if dublicates == 0:
                    return full_content_result[:1]
                else:
                    return full_content_result
            except FileNotFoundError:
                # TODO: add good exception :)
                logger.error(
                    'Can\'t read file {}, content checker failed!'.format(
                        file_path))

        check_func_array = [name_check, extention_check, content_check]

        full_result = []
        for func_num in priority:
            result = check_func_array[func_num]()
            if result:
                if dublicates == 0:
                    return result
                else:
                    full_result += result
        return full_result

    ####################
    # finder functions #
    ####################
    def password_finder(self, need_to_return=None):
        # TODO: fix def password_finder(self, need_to_return=self.to_return):
        if need_to_return == None: need_to_return = self.to_return

        logger.info('Process: finding password files..')
        logger.debug('Return info: {}, Obj val info: {}'.format(need_to_return,
                                                                self.to_return))
        result_array = []
        for file in self.files_list:
            result = self.file_checker(file,
                                       password_files,
                                       password_extentions,
                                       password_signatures,
                                       ignore_extentions_password,
                                       find_all=0,
                                       priority=[0, 1, 2],
                                       dublicates=0)
            if result != []:
                result_array.append([file, result])
        self.output_result(result_array)
        if need_to_return:
            return result_array

    def cert_finder(self, need_to_return=None):
        # TODO: fix def password_finder(self, need_to_return=self.to_return):
        if need_to_return == None: need_to_return = self.to_return

        logger.info('Process: finding certificate files..')
        logger.debug('Return info: {}, Obj val info: {}'.format(need_to_return,
                                                                self.to_return))
        result_array = []
        for file in self.files_list:
            result = self.file_checker(file,
                                       cert_files,
                                       cert_extentions,
                                       cert_signatures,
                                       ignore_extentions_cert,
                                       find_all=0,
                                       priority=[2, 0, 1],
                                       dublicates=0)
            if result != []:
                result_array.append([file, result])
        self.output_result(result_array)
        if need_to_return:
            return result_array

    def backup_finder(self, need_to_return=None):
        # TODO: fix def password_finder(self, need_to_return=self.to_return):
        if need_to_return == None: need_to_return = self.to_return

        logger.info('Process: finding backup files..')
        logger.debug('Return info: {}, Obj val info: {}'.format(need_to_return,
                                                                self.to_return))
        result_array = []
        for file in self.files_list:
            result = self.file_checker(file,
                                       backup_files,
                                       backup_extention,
                                       backup_signatures,
                                       ignore_extentions_backup,
                                       find_all=0,
                                       priority=[2, 0, 1],
                                       dublicates=0)
            if result != []:
                result_array.append([file, result])
        self.output_result(result_array)
        if need_to_return:
            return result_array

    def hash_finder(self, need_to_return=None):
        # TODO: fix def password_finder(self, need_to_return=self.to_return):
        if need_to_return == None: need_to_return = self.to_return

        logger.info('Process: finding hash files..')
        logger.debug('Return info: {}, Obj val info: {}'.format(need_to_return,
                                                                self.to_return))
        result_array = []
        for file in self.files_list:
            result = self.file_checker(file,
                                       hash_files,
                                       hash_extentions,
                                       hash_signatures,
                                       ignore_extentions_hash,
                                       find_all=1,
                                       priority=[2, 1, 0],
                                       dublicates=1)
            if result != []:
                result_array.append([file, result])
        self.output_result(result_array)
        if need_to_return:
            return result_array

    def contacts_finder(self, need_to_return=None):
        # TODO: fix def password_finder(self, need_to_return=self.to_return):
        if need_to_return == None: need_to_return = self.to_return

        logger.info('Process: finding contacts files..')
        logger.debug('Return info: {}, Obj val info: {}'.format(need_to_return,
                                                                self.to_return))
        result_array = []
        for file in self.files_list:
            result = self.file_checker(file,
                                       contacts_files,
                                       contacts_extentions,
                                       contacts_signatures,
                                       ignore_extentions_contacts,
                                       find_all=1,
                                       priority=[2, 1, 0],
                                       dublicates=1)
            if result != []:
                result_array.append([file, result])
        result_array = self.check_domain_results(result_array)
        domain_arr = {}
        self.output_result(result_array)

        if need_to_return:
            return result_array

    def check_domain_results(self, result_arr):
        domain_dict_path = './isf/firmware/goldfinder/dicts/first_level_domains.txt'
        domain_list_url = 'http://data.iana.org/TLD/tlds-alpha-by-domain.txt'
        domains_arr = []
        if not os.path.exists(domain_dict_path):
            pass
            # TODO: add autoupdate domains
            # logger.info('Downloading first-level domain list..')
            # r = requests.get(domain_list_url)
            # f = open(domain_dict_path, 'wb')
            # f.write(r.text.encode('charmap'))
            # f.close()
            # domains_arr = r.text.split('\n')
        else:
            f = open(domain_dict_path)
            domains_arr = f.read().split('\n')
            f.close()
        for result_i in range(len(result_arr))[::-1]:
            for signature_i in range(len(result_arr[result_i][1]))[::-1]:
                if 'content_signature' in result_arr[result_i][1][
                    signature_i] and result_arr[result_i][1][signature_i][
                    'content_signature'][-1] == 'Domain address':
                    for result_content_i in range(
                            len(result_arr[result_i][1][signature_i][
                                    'found_str']))[::-1]:
                        if result_arr[result_i][1][signature_i]['found_str'][
                            result_content_i].split('.')[
                            -1].lower() not in domains_arr:
                            logger.debug(
                                'DELETE: ' +
                                result_arr[result_i][1][signature_i][
                                    'found_str'][
                                    result_content_i])
                            del \
                                result_arr[result_i][1][signature_i][
                                    'found_str'][
                                    result_content_i]
                    if result_arr[result_i][1][signature_i]['found_str'] == []:
                        del result_arr[result_i][1][signature_i]
            if result_arr[result_i][1] == []:
                del result_arr[result_i]
        return result_arr

    def output_result(self, result_arr):
        logger.info('=======================================================')
        logger.info('================GoldFinder results=====================')
        logger.info('=======================================================')
        output = ''
        for result_i in result_arr:
            logger.info('File :{}'.format(result_i[0]))
            output += 'File :{}\n'.format(result_i[0])
            for signature_i in result_i[1]:
                logger.debug(str(signature_i))
                logger.info('\tSignature: {}'.format(
                    signature_i[next(iter(signature_i))][-1]))
                output += '\tSignature: {}\n'.format(
                    signature_i[next(iter(signature_i))][-1])
                for found_str in signature_i['found_str']:
                    logger.info('\t\tFound : "{}"'.format(found_str))
                    output += '\t\tFound : "{}"\n'.format(found_str)
        return output

    def full_scan(self, need_to_return=None, file_output="NONONO"):
        if need_to_return == None: need_to_return = self.to_return

        logger.info('Process: finding all types of files..')
        logger.debug('Return info: {}, Obj val info: {}'.format(need_to_return,

                                                                self.to_return))
        # first check for consists files with secrets

        full_files = password_files + cert_files + hash_files + backup_files + contacts_files
        full_extentions = password_extentions + cert_extentions + hash_extentions + backup_extention + contacts_extentions
        full_signatures = password_signatures + cert_signatures + hash_signatures + backup_signatures + contacts_signatures
        full_ignore_extentions = ignore_extentions_password + ignore_extentions_cert + ignore_extentions_hash + ignore_extentions_backup + ignore_extentions_contacts

        result_array = []
        for file in self.files_list:
            result = self.file_checker(file,
                                       full_files,
                                       full_extentions,
                                       full_signatures,
                                       full_ignore_extentions,
                                       find_all=1,
                                       priority=[0, 1, 2],
                                       dublicates=1)
            if result != []:
                result_array.append([file, result])

        result_array = self.check_domain_results(result_array)
        output = self.output_result(result_array)

        if file_output != 'NONONO':
            try:
                f = open(file_output, 'w')
                f.write(output)
                f.close()
            except NameError:
                logger.error(
                    'Error while saving result to file {}'.format(file_output))

        if need_to_return:
            return result_array
        return
