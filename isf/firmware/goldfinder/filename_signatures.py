'''
Files structure:
    path: regexp of file
    os: Unix/Windows/MacOS/any
    os_bits: x64/x86/x32/x16/x8/any
    os_description: Other OS information
    type: file/dir/link
    info_type: keys/information/vulnerable (Example: keys|information in one file)
    description: Info about file

Extention structure:
    ext: extention of file (dot included)
    os: Unix/Windows/MacOS/any
    os_bits: x64/x86/x32/x16/x8/any
    os_description: Other OS information
    type: file/dir/link
    info_type: keys/information/vulnerable (Example: keys|information in one file)
    description: Info about file with extention

Hints:
    [/\\] - windows & unix style path
    large signatures - to the end of array
    all lowercase
'''

files = []
extentions = []

#############################################################
#############################################################

backup_files = [
    (".*[/\\\\][^/\\\\]*" + "backup" + "[^/\\\\]*", "any", "any", "-", "file",
     "keys|information",
     "Interesting backup file")
]

# backup extention list
# will be added to the end of each file list

backup_extention = [
    (".bak", "any", "any", "-", "file", "information",
     "Interesting backup file"),
    ("~", "any", "any", "-", "file", "information", "Interesting backup file"),
    (".swp", "Unix", "any", "-", "file", "keys|information", "Vim backup file"),
    (".backup", "any", "any", "-", "file", "information",
     "Interesting backup file"),
    ("backup", "any", "any", "-", "file", "information",
     "Interesting backup file"),
    (".back", "any", "any", "-", "file", "information",
     "Interesting backup file"),
]

files += backup_files
extentions += backup_extention

#############################################################
#############################################################

# password files and password information
password_files = [

    #############
    # filenames #
    #############

    (".*[/\\\\][\\.]{0,1}" + "passwd", "Unix", "any", "-", "file",
     "keys|information",
     "Users list/passwords"),
    (".*[/\\\\][\\.]{0,1}" + "shadow", "Unix", "any", "-", "file",
     "keys|information",
     "Users list/passwords"),
    (".*[/\\\\][\\.]{0,1}" + "master\\.passwd", "Unix", "any", "FreeBSD", "file",
     "keys|information",
     "Users list/passwords"),
    (".*[/\\\\][\\.]{0,1}" + "pwd\\.db", "Unix", "any", "FreeBSD", "file",
     "keys|information",
     "Users list/passwords"),
    (".*[/\\\\][\\.]{0,1}" + "spwd\\.db", "Unix", "any", "FreeBSD", "file",
     "keys|information",
     "Users list/passwords"),
    (".*[/\\\\][\\.]{0,1}" + "login\\.defs", "Unix", "any", "-", "file",
     "information",
     "password policy information"),
    (".*[/\\\\][\\.]{0,1}" + "htpasswd", "Unix", "any", "-", "file", "keys",
     "HTTP password file"),
    (".*[/\\\\][\\.]{0,1}" + "sam", "Windows", "any", "-", "file", "keys",
     "Windows password file"),
    (".*[/\\\\][\\.]{0,1}" + "pass\\.dat", "Windows", "any", "-", "file", "keys",
     "Interesting password file"),
    (".*[/\\\\][\\.]{0,1}" + "password\\.log", "Windows", "any", "-", "file",
     "keys",
     "Interesting password file"),
    (".*[/\\\\][\\.]{0,1}" + "ftpd\\.passwd", "any", "any", "-", "file", "keys",
     "FTPD password file"),
    (".*[/\\\\][\\.]{0,1}" + "pureftpd\\.passwd", "any", "any", "-", "file",
     "keys",
     "PureFTPD password file"),
    (".*[/\\\\][\\.]{0,1}" + "\\.bash_history", "Unix", "any", "any", "file",
     "keys|information",
     "Users Bash console history"),

    #############################
    # file names with substring #
    #############################

    (".*[/\\\\][^/\\\\]*" + "password" + "[^/\\\\]*", "any", "any", "-", "file",
     "keys",
     "Interesting password file"),
    (".*[/\\\\][^/\\\\]*" + "passwd" + "[^/\\\\]*", "any", "any", "-", "file", "keys",
     "Interesting password file"),
    (".*[/\\\\][^/\\\\]*" + "pass" + "[^/\\\\]*", "any", "any", "-", "file", "keys",
     "Interesting password file"),
    (".*[/\\\\][^/\\\\]*" + "secret" + "[^/\\\\]*", "any", "any", "-", "file", "keys",
     "Interesting password file"),
]

password_extentions = [
    ###################
    # file extentions #
    ###################

    (".passwd", "any", "any", "-", "file", "keys",
     "Interesting password file"),
    (".psk", "any", "any", "-", "file", "keys",
     "Interesting password file"),
    (".pwd", "any", "any", "-", "file", "keys",
     "Interesting password file"),
    (".pwl", "Windows", "any", "Windows 95,98,ME", "file", "keys",
     "Old Windows password file")
]



files += password_files
extentions += password_extentions
#############################################################
#############################################################

cert_files = [
    (".*[/\\\\][\\.]{0,1}" + "[/\\\\]*id_rsa[/\\\\]*", "any", "any", "-", "file", "keys",
     "SSH key"),
    (".*[/\\\\][\\.]{0,1}" + "[/\\\\]*authorized_keys[/\\\\]*", "any", "any", "-", "file", "keys",
     "SSH key"),
    (".*[/\\\\][\\.]{0,1}" + "[/\\\\]*host_key[/\\\\]*", "any", "any", "-", "file", "keys",
     "SSH key"),
    (".*[/\\\\][\\.]{0,1}" + "[/\\\\]*id_dsa[/\\\\]*", "any", "any", "-", "file", "keys",
     "SSH key"),
]

cert_extentions = [
    (".pem", "any", "any", "-", "file", "keys",
     "Interesting certificate file"),
    (".crt", "any", "any", "-", "file", "keys",
     "Interesting certificate file"),
    (".pub", "any", "any", "-", "file", "keys",
     "Public key"),
    (".key", "any", "any", "-", "file", "keys",
     "Interesting certificate file"),
    (".p12", "any", "any", "-", "file", "keys",
     "PKCS#12 certificate file"),
    (".cer", "any", "any", "-", "file", "keys",
     "Interesting certificate file"),
    (".p7b", "any", "any", "-", "file", "keys",
     "PKCS#7 certificate file"),
]

files += cert_files
extentions += cert_extentions
#############################################################
#############################################################
# hash signatures

hash_files = [
]

hash_extentions = [
    (".hash", "any", "any", "-", "file", "keys",
     "Hash file format"),
    (".md5", "any", "any", "-", "file", "keys",
     "Md5 hash file"),
    (".sha1", "any", "any", "-", "file", "keys",
     "sha1 hash file"),
    (".sha256", "any", "any", "-", "file", "keys",
     "sha256 hash file")
]

files += cert_files
extentions += cert_extentions
#############################################################
#############################################################
# contacts signatures

contacts_files = [
]

contacts_extentions = []

files += cert_files
extentions += cert_extentions
#############################################################
#############################################################