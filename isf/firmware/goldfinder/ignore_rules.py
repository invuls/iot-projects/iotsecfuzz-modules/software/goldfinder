'''
    Tips:
        Extention with dot(.), lowercase

    Ignore Extention structure:
        Extention - extention with dot(.), lowercase
        Description - desctiption of rule
'''
ignore_extentions = set() #full list with ignored extentions

##########################################################
#                   common ignore list                   #
##########################################################

ignore_extentions_common = [
    (".htm", "No interesting info in .htm"),
    (".html", "No interesting info in .html"),
    (".css", "No interesting info in .css"),
]

ignore_extentions.update(ignore_extentions_common)
##########################################################
#              password files ignore list                #
##########################################################

ignore_extentions_password = [

]

ignore_extentions.update(ignore_extentions_password)
ignore_extentions_password += ignore_extentions_common

##########################################################
#                cert files ignore list                  #
##########################################################

ignore_extentions_cert = []
ignore_extentions.update(ignore_extentions_cert)
ignore_extentions_cert += ignore_extentions_common

##########################################################
#                hash files ignore list                  #
##########################################################

ignore_extentions_hash = []
ignore_extentions.update(ignore_extentions_hash)
ignore_extentions_hash += ignore_extentions_common


##########################################################
#              contacts files ignore list                #
##########################################################

ignore_extentions_contacts = []
ignore_extentions.update(ignore_extentions_contacts)

#Not ignore - there may be contacts in html and so on
#ignore_extentions_contacts += ignore_extentions_common

##########################################################
#              contacts files ignore list                #
##########################################################

ignore_extentions_backup = []
ignore_extentions.update(ignore_extentions_backup)
